//
//  CashRegister.swift
//  Whack-AR-Mole
//
//  Created by Tinkertanker on 2/12/19.
//  Copyright © 2019 Tinkercademy. All rights reserved.
//

import Foundation
import RealityKit
import UIKit
import ARKit

class Field: Entity, HasModel, HasCollision {
    
    required init() {
        super.init()
        self.components[ModelComponent] = ModelComponent(
            mesh: .generateBox(size: [10, 0, 10]),
            materials: [SimpleMaterial(color: .gray, roughness: .init(integerLiteral: 20), isMetallic: false)
        ]
        )
        // Generate collision shapes
        // I believe this should only changed to custom collision shapes if you want to customise the hitbox of stuff
        self.generateCollisionShapes(recursive: true)
    }
    
    
}

