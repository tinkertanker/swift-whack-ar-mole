//
//  ViewController.swift
//  Whack-AR-Mole
//
//  Created by Soon Yin Jie & Sean Wong on 2/12/19.
//  Copyright © 2019 Tinkercademy. All rights reserved.
//

import UIKit
import RealityKit

class ViewController: UIViewController {
    @IBOutlet var arView: ARView!
    var growNotifications: [Experience.NotificationTrigger]! = []
    var shrinkNotifications: [Experience.NotificationTrigger]! = []
    var boxes: [Entity]! = []
    var score = 0
    var time = 30.0
    var isGameRunning = false
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* MARK: First Setup */
        // Initialise AR View
        initialiseARView()
        
        // Get the "Game" scene
        let scene = arView.scene.anchors[0] as! Experience.Game
        
        // Set up notifications
        shrinkNotifications = [scene.notifications.shrink1,scene.notifications.shrink2,scene.notifications.shrink3,scene.notifications.shrink4,scene.notifications.shrink5]
        
        growNotifications = [scene.notifications.grow1,scene.notifications.grow2,scene.notifications.grow3,scene.notifications.grow4,scene.notifications.grow5]
        
        // Init tap gesture recogniser
        let tapGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        arView.addGestureRecognizer(tapGestureRecogniser)
        
        let field = Field()
        scene.addChild(field)
        
        // Put a label into the AR View
        let label = UILabel(frame: CGRect(x: arView.frame.width - 130, y: 30
            , width: 100.0, height: 300.0))
        label.text = String(time)
        label.tag = 300
        label.isHidden = true
        arView.addSubview(label)
        
        /* MARK: Use Notify Actions to make boxes pop up */
        // This closure is called when the shrink is finished.
        // The notify action is set in the experience scene.
        // It starts a timer for 3 seconds and then sends a notification to grow the box again.
        
        scene.actions.finishShrink.onAction = { (box) in
            let unwrappedBox = box!
            // Timing is done in reality composer
            for (index, eachBox) in self.boxes.enumerated() {
                if eachBox.name == unwrappedBox.name && self.isGameRunning  {
                    self.growNotifications[index].post()
                    }
                }
                self.score += 1
            }

        
        /* MARK: Notify Action On Game Start */
        
        scene.actions.gameStarted.onAction = { (_) in
            self.isGameRunning = true
            let timerLabel = self.arView.viewWithTag(300) as! UILabel
            timerLabel.isHidden = false
            timerLabel.text = String(self.time)
            timerLabel.transform = CGAffineTransform(scaleX: 0, y: 0)
            
            let animator = UIViewPropertyAnimator(duration: 0.5, curve: .easeInOut) {
                timerLabel.transform = CGAffineTransform.identity
            }
            animator.startAnimation()
            
            let scene = self.arView.scene.anchors[0] as! Experience.Game
            let timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { (tim) in
                
                if self.time <= 0 {
                
                    timerLabel.text = "Time's up!"
                    timerLabel.numberOfLines = 0
                    let defaults = UserDefaults.standard
                    
                    self.isGameRunning = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        let highestScore = defaults.integer(forKey: "highestScore")
                        if self.score > highestScore {
                            timerLabel.text = "NEW HIGH SCORE! Your score is \(self.score)!"
                            defaults.set(self.score, forKey: "highestScore")
                        } else {
                            timerLabel.text = "Your score is \(self.score)!"
                        }
                        
                        self.score = 0
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        let animator = UIViewPropertyAnimator(duration: 1, curve: .easeInOut) {
                            timerLabel.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                        
                        }
                        animator.startAnimation()
                        scene.notifications.restartGame.post()
                        self.time = 30.0
                    }
                    
                    tim.invalidate()
                } else {
                    
                    self.time -= 0.01
                    self.time = round(self.time*100)/100
                    timerLabel.text = "⏱: " + String(self.time)
                    
                }
            }
        }
        scene.actions.gameRestarted.onAction = { (_) in
            print("it restarted")
            scene.notifications.gameEnded.post()
        }
        
    }
    func initialiseARView() {
        /* MARK: Initialise AR View */
        
        // Get the anchor from experience file
        let sceneAnchor = try! Experience.loadGame()
        
        // Add it to anchor
        arView.scene.anchors.append(sceneAnchor)
        
        // Get the scene as the "Game" scene
        // We will do this multiple times throughout the code
        let scene = arView.scene.anchors[0] as! Experience.Game
        
        // Initialise boxes
        boxes = [scene.box1!,scene.box2!,scene.box3!,scene.box4!,scene.box5!]
        
       
        
        
    }
    
    @IBAction func handleTap(_ sender: UITapGestureRecognizer) {
        
        // Do nothing if the game is over
        if time <= 0 { return }
        
        /* MARK: Hit Detection for Entities */
        
        // you MAY need to guard let the tapped area if you have other viewcontrollers
        // in our case the entire screen is an ARView so we don't have to worry about the tap gesture recogniser not tapping inside
        
        // get the tapped area
        let touchInView = sender.location(in: arView)
        guard let hitEntity = arView.entity(at: touchInView)
        else { return } // because no entity was found
        
        
        for i in 0 ..< boxes.count  {
            /*
             Get the box, then shrink it
             Don't do anything if a box is not touched
             */
            if hitEntity.name == boxes[i].name {
                    shrinkNotifications[i].post()
                }
    
        }
    }


}
