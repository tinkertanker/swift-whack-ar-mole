//
//  ViewController.swift
//  Clicker
//
//  Created by Sean on 30/6/18.
//  Copyright © 2018 Sean. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var startLabel: UILabel!
    @IBAction func startButtonPressed(_ sender: Any) {
        let animator = UIViewPropertyAnimator(duration: 0.3, curve: .easeInOut) {
            self.startLabel.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            self.scoreLabel.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }
        animator.startAnimation()
        animator.addCompletion { (_) in
            self.performSegue(withIdentifier: "startSegue", sender: Any.self)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        scoreLabel.transform = CGAffineTransform.identity
        startLabel.transform = CGAffineTransform.identity
        print(startLabel.frame.maxY)
        let defaults = UserDefaults.standard
        let score = defaults.integer(forKey: "highestScore")
        scoreLabel.text = "High score: \(score)!"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scoreLabel.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        startLabel.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        let animator = UIViewPropertyAnimator(duration: 1.0, curve: .easeInOut) {
            self.scoreLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.startLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.scoreLabel.transform = CGAffineTransform.identity
            self.startLabel.transform = CGAffineTransform.identity
        }
        animator.startAnimation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

